# Hora Vera

A Roman Clock.  

## Getting Started

### Requirements
Xcode 11.3

### Building

No dependencies need to be installed. Open and build ios/horavera/horavera.xcode.

## Author

**Philip White**

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Details

### What is it?
Hora Vera (which means "Real Time" in Latin) is a solar clock. Some ancient civilizations, such as the Romans, divided the time between sunrise and sunset into 12 periods called hours. Since the length of the daylight period varies with the time of year and with one's location on the planet, the length of a Roman hour varies as well. This is in contrast to the conventional modern hour, which is of a fixed length. On the equinox, when the daytime and nighttime each comprise exactly half of the day, the Roman hour is equal in length to the conventional hour.

The Romans commonly divided nighttime into 4 equal periods, called watches or vigils. At times, the Romans and other ancients may have divided the night into 12 hours like the day, and the day into 4 watches like the night. Hora Vera can be configured to show any of these divisions.

### Why should such a thing exist?
I initially developed this app as an aid in prayer. In traditional Western Christianity, the daily cycle of prayers is called the Divine Office, or the Liturgy of the Hours. It consists of 8 parts, called hours, intended to be prayed at specific times: sunrise, midday, sunset, etc. Indeed some of these bear names indicating at which Roman hour of the day they should be prayed: e.g. Terce at the third hour of daylight, None at the ninth. In modern usage, most people simply pray these at fixed times on the conventional clock, but I find it pleasing to pray these hours near their originally intended times.

Also, in some traditions, feast days and fast days start or end at sunrise or sunset. This app is useful to know when to mark those events. Finally, for the soul who finds herself stuck in a windowless office, this app may provide a helpful window into the day's progress.

### How does it work?
Hora Vera needs your location to determine when the sun rises and sets. It also needs access to the internet since it doesn't calculate the sunrise and sunset itself, but fetches the information from https://sunrise-sunset.org. It doesn't need continuous access to the internet, but it does need to refresh it's data daily and also if you travel more than 80 miles or so since the last refresh.


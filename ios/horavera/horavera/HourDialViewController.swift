//
//  FirstViewController.swift
//  horavera
//
//  Created by Philip White on 11/11/18.
//  Copyright © 2018 Philip White. All rights reserved.
//

import UIKit
import SwiftUI

class HourDialViewController: UIViewController, UIViewControllerTransitioningDelegate {

	@IBOutlet weak var dialView: HourDialView!
	@IBOutlet weak var morningTwilightSlider: UISlider!
	@IBOutlet weak var eveningTwilightSlider: UISlider!
	@IBOutlet weak var daytimeAngleSlider: UISlider!
	@IBOutlet weak var currentTimeAngleSlider: UISlider!
	@IBOutlet weak var toolbar : UIToolbar!
	
	var lastUpdateTime : Date? = nil
	var inErrorState : Bool = false
	
	var inDebugMode : Bool = false
	var debugTotalDaytimeAngle : CGFloat = .pi
	var debugCurrentTimeAngle : CGFloat = 0.0
	var debugMorningTwilightAngle : CGFloat = .pi / 8.0
	var debugEveningTwilightAngle : CGFloat = .pi / 8.0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		NotificationCenter.default.addObserver(self, selector: #selector(onSolarManagerDidUpdate), name: .solarManagerDidUpdate, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(onClockTick), name: .clockTick, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(displaySettingsDidChange), name: .displaySettingsDidChange, object: nil)
		
		NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
		
		NotificationCenter.default.addObserver(self, selector:#selector(didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
		
		morningTwilightSlider.isHidden = true
		eveningTwilightSlider.isHidden = true
		daytimeAngleSlider.isHidden = true
		currentTimeAngleSlider.isHidden = true
		currentTimeAngleSlider.minimumValue = -0.5
		currentTimeAngleSlider.maximumValue = 1.5
		
		// Add a gesture recognizer to enter or exit test mode
		#if DEBUG
		let tripleTapRecognizer = UITapGestureRecognizer(target:self, action:#selector(backgroundTripleTapped))
		tripleTapRecognizer.numberOfTapsRequired = 3;
		view.addGestureRecognizer(tripleTapRecognizer)
		#endif
		// Single tap to show toolbar
		let singleTapRecognizer = UITapGestureRecognizer(target:self, action:#selector(backgroundTapped))
		singleTapRecognizer.numberOfTapsRequired = 1;
		view.addGestureRecognizer(singleTapRecognizer)
		
		// Configure display settings
		let settings = Settings()
		dialView.showCenterCross = settings.showCenterCross
		dialView.showDayHours = settings.showDayHours
		dialView.showDayWatches = settings.showDayWatches
		dialView.showNightHours = settings.showNightHours
		dialView.showNightWatches = settings.showNightWatches
	}
	
	@objc func backgroundTapped(recognizer: UITapGestureRecognizer) {
		toolbar.isHidden = !toolbar.isHidden
	}
	
	@IBAction func showInfoPage() {
		var infoPageController : UIHostingController<InfoPageView>?
		infoPageController = UIHostingController(rootView: InfoPageView { infoPageController!.dismiss(animated: true, completion: nil) })
		self.show(infoPageController!, sender:nil)
	}
	
	@IBAction func showSettings() {
		let settingsViewController = SettingsViewController()
		settingsViewController.transitioningDelegate = self
		settingsViewController.modalPresentationStyle = .custom
		self.show(settingsViewController, sender: nil)
	}
	// UIViewControllerTransitioningDelegate
	func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
		return SettingsPresentationController(presentedViewController: presented, presenting: presenting)
	}
	
	// debug methods (sliders)

	@objc func backgroundTripleTapped(recognizer: UITapGestureRecognizer) {
		inDebugMode = !inDebugMode
		
		morningTwilightSlider.isHidden = !inDebugMode
		eveningTwilightSlider.isHidden = !inDebugMode
		daytimeAngleSlider.isHidden = !inDebugMode
		currentTimeAngleSlider.isHidden = !inDebugMode
		
		if (inDebugMode) {
			morningTwilightSlider.value = Float(debugMorningTwilightAngle) / (0.5 * .pi)
			eveningTwilightSlider.value = Float(debugEveningTwilightAngle) / (0.5 * .pi)
			daytimeAngleSlider.value = Float(debugTotalDaytimeAngle / (2 * .pi))
			currentTimeAngleSlider.value = Float(debugCurrentTimeAngle / (2 * .pi))
		
			dialView.setTotalDaytimeAngle(debugTotalDaytimeAngle,
										  currentTimeAngle: debugCurrentTimeAngle,
										  morningTwilightAngle: debugMorningTwilightAngle,
										  eveningTwilightAngle: debugEveningTwilightAngle,
										  animate:false)
		} else {
			let appDelegate = UIApplication.shared.delegate as! AppDelegate?
			let horaVera = appDelegate?.solarManager?.horaVera()
			configureViewWithHoraVera(horaVera)
		}
	}
	
	@IBAction func debugSliderDidChange(sender: UISlider) {
		switch sender {
		case morningTwilightSlider:
			debugMorningTwilightAngle = 0.5 * .pi * CGFloat(morningTwilightSlider.value)
			break
		case eveningTwilightSlider:
			debugEveningTwilightAngle = 0.5 * .pi * CGFloat(eveningTwilightSlider.value)
			break
		case daytimeAngleSlider:
			debugTotalDaytimeAngle = 2 * .pi * CGFloat(daytimeAngleSlider.value)
		case currentTimeAngleSlider:
			var value = CGFloat(currentTimeAngleSlider.value)
			if value < 0 {
				value = 1.0 + value
			} else if value > 1 {
				value = value - 1.0
			}
			debugCurrentTimeAngle = 2 * .pi * value
		default:
			break
		}
		
		dialView.setTotalDaytimeAngle(debugTotalDaytimeAngle,
									  currentTimeAngle: debugCurrentTimeAngle,
									  morningTwilightAngle: debugMorningTwilightAngle,
									  eveningTwilightAngle: debugEveningTwilightAngle,
									  animate:false)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@objc func onSolarManagerDidUpdate(_ notification: Notification) {
		let appDelegate = UIApplication.shared.delegate as! AppDelegate?
		
		if let solarManager = appDelegate?.solarManager {
			switch solarManager.state {
			case .initializing:
				break
			case .ready:
				configureViewWithHoraVera(solarManager.horaVera())
			case .failure: // will be nil
				if (!inErrorState) {
					inErrorState = true
					
					var errorMessageKey = ""
					
					switch (solarManager.errorType) {
					case .locationGeneralError:
						errorMessageKey = "LocationGeneralFailureMessage"
						break
					case .locationAuthenticationError:
						errorMessageKey = "LocationAuthenticationFailureMessage"
						break
					case .timeServiceFailure:
						errorMessageKey = "TimeServiceErrorMessage"
						break
					default:
						errorMessageKey = "UnknownErrorMessage"
					}
					
					let errorMessage = NSLocalizedString(errorMessageKey, comment: "")
					let errorTitle = NSLocalizedString("ErrorTitle", comment: "")
					let buttonTitle = NSLocalizedString("ErrorButton", comment: "")
					
					let alert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
					
					var token : NSObjectProtocol?
					token = NotificationCenter.default.addObserver(forName: UIApplication.willResignActiveNotification, object: nil, queue: nil) { (_) in
						alert.dismiss(animated: true, completion: nil)
						NotificationCenter.default.removeObserver(token!)
					}
					
					alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { _ in
						self.inErrorState = false
						appDelegate?.reset()
					}))
					
					if (solarManager.errorType == .locationAuthenticationError) {
						let settingsButtonTitle = NSLocalizedString("LocationSettingsButton", comment: "")
						alert.addAction(UIAlertAction(title: settingsButtonTitle, style: .default, handler: { _ in
								
							if let url = URL(string: UIApplication.openSettingsURLString) {
								UIApplication.shared.open(url, options:[:], completionHandler: nil)
							}
						}))
					}
					
					present(alert, animated: true, completion: nil)
				}
			}
		}
	}
	
	@objc func onClockTick(_ notification: Notification) {
		// We should be on the main thread already
		let appDelegate = UIApplication.shared.delegate as! AppDelegate?
		
		if let solarManager = appDelegate?.solarManager {
			switch solarManager.state {
			case .initializing:
				break
			case .ready:
				configureViewWithHoraVera(solarManager.horaVera())
			case .failure:
				// Do nothing here, failure case will be handled in onSolarManagerDidUpdate
				break
			}
		}
	}
	
	@objc func displaySettingsDidChange(_ notification: Notification) {
		let settings = Settings()
		dialView.showCenterCross = settings.showCenterCross
		dialView.showDayHours = settings.showDayHours
		dialView.showDayWatches = settings.showDayWatches
		dialView.showNightHours = settings.showNightHours
		dialView.showNightWatches = settings.showNightWatches
	}
	
	@objc func didBecomeActive(_ notification: Notification) {
		if inErrorState {
			inErrorState = false
			let appDelegate = UIApplication.shared.delegate as! AppDelegate?
			appDelegate?.reset()
		}
	}
	
	@objc func didEnterBackground(_ notification: Notification) {
		toolbar.isHidden = true
	}
	
	func configureViewWithHoraVera(_ horaVera: HoraVera!) {
	
		if inDebugMode {
			return
		}
		
		if let horaVera = horaVera {
			let totalDaytimeAngle = 2.0 * .pi * CGFloat(horaVera.daytimeFraction)
			let totalNighttimeAngle = 2.0 * .pi - totalDaytimeAngle
			var currentTimeAngle : CGFloat = 0.0
			let morningTwilightAngle : CGFloat = CGFloat(horaVera.morningTwilightFraction) * 2.0 * .pi
			let eveningTwilightAngle : CGFloat = CGFloat(horaVera.eveningTwilightFraction) * 2.0 * .pi
			
			if horaVera.stage == .dayTime {
				currentTimeAngle = totalNighttimeAngle + CGFloat(horaVera.hour) * totalDaytimeAngle / 12.0
			} else {
				currentTimeAngle = CGFloat(horaVera.hour) * totalNighttimeAngle / 12.0
			}
			dialView.setTotalDaytimeAngle(totalDaytimeAngle,
										  currentTimeAngle: currentTimeAngle,
										  morningTwilightAngle: morningTwilightAngle,
										  eveningTwilightAngle: eveningTwilightAngle,
										  animate: true)
			
			// Store values as debug values as well so if we go into debug mode it is set to the current time
			debugTotalDaytimeAngle = totalDaytimeAngle
			debugCurrentTimeAngle = currentTimeAngle
			debugMorningTwilightAngle = morningTwilightAngle
			debugEveningTwilightAngle = eveningTwilightAngle
		}
	}
}


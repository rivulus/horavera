//
//  SettingsViewController.swift
//  horavera
//
//  Created by Philip White on 10/15/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
	
	@IBOutlet var showCenterCross : UISwitch!
	@IBOutlet var showDayHours : UISwitch!
	@IBOutlet var showDayWatches : UISwitch!
	@IBOutlet var showNightHours : UISwitch!
	@IBOutlet var showNightWatches : UISwitch!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
        // Do any additional setup after loading the view.
		showCenterCross.isOn = Settings().showCenterCross
		showDayHours.isOn = Settings().showDayHours
		showDayWatches.isOn = Settings().showDayWatches
		showNightHours.isOn = Settings().showNightHours
		showNightWatches.isOn = Settings().showNightWatches
    }
	
	@IBAction func changeDisplaySetting(_ sender : UIControl) {
		let settings = Settings()
		let state = (sender as! UISwitch).isOn
		switch sender {
		case showCenterCross:
			settings.showCenterCross = state
			break
		case showDayHours:
			settings.showDayHours = state
			if state == false { // Both watches and hours cannot be off
				settings.showDayWatches = true
				showDayWatches.setOn(true, animated: true)
			}
			break
		case showDayWatches:
			settings.showDayWatches = state
			if state == false { // Both watches and hours cannot be off
				settings.showDayHours = true
				showDayHours.setOn(true, animated: true)
			}
			break
		case showNightHours:
			settings.showNightHours = state
			if state == false { // Both watches and hours cannot be off
				settings.showNightWatches = true
				showNightWatches.setOn(true, animated: true)
			}
			break
		case showNightWatches:
			settings.showNightWatches = state
			if state == false { // Both watches and hours cannot be off
				settings.showNightHours = true
				showNightHours.setOn(true, animated: true)
			}
			break
		default:
			assertionFailure("Unexpected sender")
		}
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

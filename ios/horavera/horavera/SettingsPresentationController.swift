//
//  SettingsPresentationController.swift
//  horavera
//
//  Created by Philip White on 10/16/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import UIKit
class SettingsPresentationController: UIPresentationController{
	let backgroundOverlayView: UIView!
	var tapGestureRecognizer: UITapGestureRecognizer!
	
	@objc func dismiss(){
		presentedViewController.dismiss(animated: true, completion: nil)
	}
	
	override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
		backgroundOverlayView = UIView()
		super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
		tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismiss))
		backgroundOverlayView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		backgroundOverlayView.isUserInteractionEnabled = true
		backgroundOverlayView.addGestureRecognizer(tapGestureRecognizer)
	}
	
	override var frameOfPresentedViewInContainerView: CGRect{
		let height = presentedView!.frame.height
		return CGRect(origin: CGPoint(x: 0, y: containerView!.frame.height - height), size: CGSize(width: containerView!.frame.width, height:height))
	}
	
	override func dismissalTransitionWillBegin() {
		presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) in
			self.backgroundOverlayView.alpha = 0
		}, completion: { (UIViewControllerTransitionCoordinatorContext) in
			self.backgroundOverlayView.removeFromSuperview()
		})
	}
	
	override func presentationTransitionWillBegin() {
		backgroundOverlayView.alpha = 0
		containerView?.addSubview(backgroundOverlayView)
		presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) in
			self.backgroundOverlayView.alpha = 1
		}, completion: { (UIViewControllerTransitionCoordinatorContext) in
			
		})
	}
	
	override func containerViewWillLayoutSubviews() {
		super.containerViewWillLayoutSubviews()
		presentedView!.layer.masksToBounds = true
		presentedView!.layer.cornerRadius = 10
	}
	
	override func containerViewDidLayoutSubviews() {
		super.containerViewDidLayoutSubviews()
		presentedView?.frame = frameOfPresentedViewInContainerView
		backgroundOverlayView.frame = containerView!.bounds
	}
}

//
//  SunData.swift
//  horavera
//
//  Created by Philip White on 12/3/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import Foundation
import CoreLocation

struct SunTimes : Codable {
	let morningTwilight : Date
	let sunrise : Date
	let sunset : Date
	let eveningTwilight : Date
}

struct SunData {
	let timeStamp : Date
	let location: CLLocation
	var yesterday: SunTimes?
	var today: SunTimes?
	var tomorrow: SunTimes?
	
	var latitude: CLLocationDegrees? {
		get {
			return location.coordinate.latitude
		}
	}
	var longitude: CLLocationDegrees? {
		get {
			return location.coordinate.longitude
		}
	}
	
	func inAcceptableRange(of currentLocation : CLLocation) -> Bool {
		let distance = currentLocation.distance(from: location)
		return distance < 100000 //100km
	}
	
	var isOutOfDate : Bool {
		get {
			return Calendar.current.isDateInToday(timeStamp)
		}
	}
	
	var isComplete : Bool {
		get {
			return yesterday != nil &&
				today != nil &&
				tomorrow != nil
		}
	}
	
	var isCoherent : Bool {
		get {
			assert(isComplete)
			return yesterday!.eveningTwilight <= today!.morningTwilight &&
				today!.eveningTwilight <= tomorrow!.morningTwilight
		}
	}
	
	/*var hasTwilightData : Bool {
		get {
			if priorDayEveningTwilight!.timeIntervalSinceReferenceDate < 0 ||
			morningTwilight!.timeIntervalSinceReferenceDate < 0 ||
				eveningTwilight!.timeIntervalSinceReferenceDate	< 0 ||
				nextDayMorningTwilight!.timeIntervalSinceReferenceDate < 0 {
				return false
			}
			return true
		}
	}
	
	mutating func interpolateTwilightData() {
		// If in a northen or southern region where twilight has no end
		// interpolate twilight as being halfway between sunrise and sunset.
		let lastNightLength = sunrise!.timeIntervalSince(priorDaySunset!)
		let lastTwilight = priorDaySunset! + lastNightLength/2.0
		
		priorDayEveningTwilight = lastTwilight
		morningTwilight = lastTwilight
		
		let nextNightLength = nextDaySunrise!.timeIntervalSince(sunset!)
		let nextTwilight = sunset! + nextNightLength/2.0
		
		eveningTwilight = nextTwilight
		nextDayMorningTwilight = nextTwilight
	}*/
	
	func writeToUserDefaults() {
		assert(isComplete)
		let userDefaults = UserDefaults.standard
		
		userDefaults.set(latitude, forKey:"currentLatitude")
		userDefaults.set(longitude, forKey:"currentLongitude")
	
		let encoder = JSONEncoder()
		try! userDefaults.set(encoder.encode(yesterday),forKey:"yesterday")
		try! userDefaults.set(encoder.encode(today),forKey:"today")
		try! userDefaults.set(encoder.encode(tomorrow),forKey:"tomorrow")
		
		userDefaults.set(timeStamp.timeIntervalSinceReferenceDate, forKey:"fetchedDate")
	}

	static func loadFromUserDefaults() -> SunData? {
		let userDefaults = UserDefaults.standard
		
		let fetchedDateTimeInterval = userDefaults.double(forKey: "fetchedDate")
		if fetchedDateTimeInterval == 0 {
			return nil // No saved data
		}
		
		let lastFetchedDate = Date(timeIntervalSinceReferenceDate:fetchedDateTimeInterval)
		
		let latitude = userDefaults.double(forKey: "currentLatitude")
		let longitude = userDefaults.double(forKey: "currentLongitude")
		let location = CLLocation(latitude: latitude, longitude: longitude)
		
		var sunData = SunData(timeStamp: lastFetchedDate, location:location)
		
		let decoder = JSONDecoder()
		do {
			if let data = userDefaults.data(forKey: "yesterday") {
				sunData.yesterday = try decoder.decode(SunTimes.self, from:data )
			}
			
			if let data = userDefaults.data(forKey: "today") {
				sunData.today = try decoder.decode(SunTimes.self, from:data )
			}
			
			if let data = userDefaults.data(forKey: "tomorrow") {
				sunData.tomorrow = try decoder.decode(SunTimes.self, from:data )
			}
		} catch {
			print("Error decoding user defaults")
			return nil
		}
		assert(sunData.isComplete)
		assert(sunData.isCoherent)
		
		return sunData
	}
	
	init(location: CLLocation) {
		timeStamp = Date()
		self.location = location
	}
	
	init(timeStamp: Date, location: CLLocation) {
		self.timeStamp = timeStamp
		self.location = location
	}
}

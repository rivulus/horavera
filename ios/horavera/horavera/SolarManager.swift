//
//  SolarManager.swift
//  horavera
//
//  Created by Philip White on 11/11/18.
//  Copyright © 2018 Philip White. All rights reserved.
//

import UIKit
import CoreLocation

extension Notification.Name {
	static let solarManagerDidUpdate = Notification.Name("solarManagerDidUpdate")
}

enum RelativeDay {
	case yesterday, today, tomorrow
}

enum DayStage {
	case dayTime, nightTime
}

enum State {
	case initializing, failure, ready
}

enum ErrorType {
	case noError, locationGeneralError, locationAuthenticationError, timeServiceFailure
}

struct HoraVera {
	let stage : DayStage
	let hour : Double // 0 <= hour < 12.0
	let daytimeFraction : Double // How long the daytime is as part of the whole day length
	let morningTwilightFraction : Double // How long the morning twilight is as part of the whole day length
	let eveningTwilightFraction : Double // How long the evening twilight is as part of the whole day length
}

class SolarManager: NSObject, CLLocationManagerDelegate {
	let forceReset = false
	var timeOffsetOverride : TimeInterval? // For testing, shift the current time by a certain amount
	
	var locationManager: CLLocationManager?
	
	var currentSunData : SunData?
	
	var attemptedLoadFromUserDefaults: Bool = false
	var state: State = .initializing
	var errorType: ErrorType = .noError
	var isFetching: Bool = false
	
	override init() {
		super.init()
		
		locationManager = CLLocationManager()
		locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager?.distanceFilter = 3000 //meters
		locationManager?.requestWhenInUseAuthorization()
		locationManager?.startUpdatingLocation()
	}
	
	// initialize with mock data
	init(yesterdaySunset: Date, yesterdayEveningTwilight: Date, morningTwilight: Date, sunrise: Date, sunset: Date, eveningTwilight: Date, tomorrowMorningTwilight: Date, tomorrowSunrise: Date) {
		super.init()
		var sunData = SunData(location:CLLocation())
	//TODO
		/*sunData.priorDaySunset = yesterdaySunset
		sunData.priorDayEveningTwilight = yesterdayEveningTwilight
		sunData.morningTwilight = morningTwilight
		sunData.sunrise = sunrise
		sunData.sunset = sunset
		sunData.eveningTwilight = eveningTwilight
		sunData.nextDayMorningTwilight = tomorrowMorningTwilight
		sunData.nextDaySunrise = tomorrowSunrise*/
		currentSunData = sunData
	}
	
	func isHoraVeraReady() -> Bool {
		return currentSunData?.isComplete ?? false
	}
	
	func horaVera(_ date: Date = Date()) -> HoraVera? {
		
		if currentSunData?.isComplete ?? false,
			let data = currentSunData,
			let yesterday = data.yesterday,
			let today = data.today,
			let tomorrow = data.tomorrow {
			
			let dayLengthInSeconds : TimeInterval = 24*60*60
			var daytimeLengthInSeconds : TimeInterval = 0
			var stage: DayStage = .dayTime
			var hour : Double = 0.0
			var morningTwilightFraction : Double = 0.0
			var eveningTwilightFraction : Double = 0.0
			
			// These variables hold the relevant times for the current calculation
			// They differ depending of what stage we're in.
			var sunrise : Date!
			var sunset : Date!
			var morningTwilight: Date!
			var eveningTwilight: Date!
			
			#if DEBUG
			var date = date
			if let timeOffsetOverride = timeOffsetOverride {
				date.addTimeInterval(timeOffsetOverride)
			}
			#endif
			
			switch date {
			case _ where date >= yesterday.sunrise && date < yesterday.sunset:
				// daytime, not sure the service will ever give us these dates,
				// but just in case
				stage = .dayTime
				sunrise = yesterday.sunrise
				sunset = yesterday.sunset
				morningTwilight = yesterday.morningTwilight
				eveningTwilight = yesterday.eveningTwilight
				
			case _ where date >= yesterday.sunset && date < today.sunrise:
				// night time (A.M.)
				stage = .nightTime
				sunrise = today.sunrise
				sunset = yesterday.sunset
				morningTwilight = today.morningTwilight
				eveningTwilight = yesterday.eveningTwilight
				
			case _ where date >= today.sunrise && date < today.sunset:
				// day time
				stage = .dayTime
				sunrise = today.sunrise
				sunset = today.sunset
				morningTwilight = today.morningTwilight
				eveningTwilight = today.eveningTwilight
				
			case _ where date >= today.sunset && date < tomorrow.sunrise:
				// night time (P.M.)
				stage = .nightTime
				sunrise = tomorrow.sunrise
				sunset = today.sunset
				morningTwilight = tomorrow.morningTwilight
				eveningTwilight = today.eveningTwilight
				
			case _ where date >= tomorrow.sunrise && date <= tomorrow.sunset:
				// daytime — unclear what the service considers to be today,
				// sometimes we might get tomorrow's times
				stage = .dayTime
				sunrise = tomorrow.sunrise
				sunset = tomorrow.sunset
				morningTwilight = tomorrow.morningTwilight
				eveningTwilight = tomorrow.eveningTwilight
				
			default:
				print("Sun dates seem wrong relative to today's date")
				return nil
			}
			
			if stage == .dayTime {
				daytimeLengthInSeconds = sunset.timeIntervalSince(sunrise)
				let secondsSinceSunrise = date.timeIntervalSince(sunrise)
				hour = 12.0 * (secondsSinceSunrise / daytimeLengthInSeconds)
			} else {
				 //.nightTime
				let nightTimeLengthInSeconds = sunrise.timeIntervalSince(sunset)
				daytimeLengthInSeconds = dayLengthInSeconds - nightTimeLengthInSeconds
				let secondsSinceSunset = date.timeIntervalSince(sunset)
				hour = 12.0 * (secondsSinceSunset / nightTimeLengthInSeconds)
			}

			morningTwilightFraction = sunrise.timeIntervalSince(morningTwilight) / dayLengthInSeconds
			eveningTwilightFraction = eveningTwilight.timeIntervalSince(sunset) / dayLengthInSeconds
			
			return HoraVera(stage: stage,
							hour: hour,
							daytimeFraction: daytimeLengthInSeconds / dayLengthInSeconds,
							morningTwilightFraction: morningTwilightFraction,
							eveningTwilightFraction: eveningTwilightFraction)
		} else {
			return nil
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if isFetching {
			return
		}
		isFetching = true
		
		// n.b. called on main thread since we started LocationManager updates from main thread
		if let location = locations.last {
			// Do this after getting a location
			loadDataFromUserDefaultsIfNecessary(currentLocation: location)
			
			// see if we should reset the state to initializing if the
			// location changed enough
			if state == .ready, let currentSunData = currentSunData {
				if !currentSunData.inAcceptableRange(of: location) {
					state = .initializing
				}
			}
			
			let pendingSunData = SunData(location:location)
			fetchSolarTimes(for: .yesterday, sunData: pendingSunData) { (pendingSunData, errorType) in
				if errorType != .noError {
					assert(errorType == .timeServiceFailure)
					self.failTimeServiceUpdate(atLocation: location)
					return
				}
				self.fetchSolarTimes(for: .today, sunData: pendingSunData) { (pendingSunData, errorType) in
					if errorType != .noError {
						assert(errorType == .timeServiceFailure)
						self.failTimeServiceUpdate(atLocation: location)
						return
					}
					self.fetchSolarTimes(for: .tomorrow, sunData: pendingSunData) { (pendingSunData, errorType) in
						if errorType != .noError {
							assert(errorType == .timeServiceFailure)
							self.failTimeServiceUpdate(atLocation: location)
						} else if self.currentSunData == nil || pendingSunData.timeStamp > self.currentSunData!.timeStamp {
							self.currentSunData = pendingSunData
							self.saveDataToUserDefaults()
							self.completeInitiation(fromCache:false)
						}
					}
				}
			}
		} else {
			failInitialization(.locationGeneralError)
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		print("Error while updating location " + error.localizedDescription)
		
		let authStatus = CLLocationManager.authorizationStatus()
		
		if (authStatus != .authorizedAlways && authStatus != .authorizedWhenInUse) {
			failInitialization(.locationAuthenticationError)
		} else {
			failInitialization(.locationGeneralError)
		}
	}
	
	func failInitialization(_ type: ErrorType) {
		assert(Thread.isMainThread)
		isFetching = false
		state = .failure
		errorType = type
		NotificationCenter.default.post(name: .solarManagerDidUpdate, object: self)
	}
	
	func failTimeServiceUpdate(atLocation location : CLLocation) {
		assert(Thread.isMainThread)
		isFetching = false
		// If we have good enough data, don't elevate to error condition
		if state != .ready {
			state = .failure
			errorType = .timeServiceFailure
			NotificationCenter.default.post(name: .solarManagerDidUpdate, object: self)
		}
	}
	
	func completeInitiation(fromCache: Bool) {
		assert(Thread.isMainThread)
		if !fromCache {
			isFetching = false
		}
		state = .ready
		NotificationCenter.default.post(name: .solarManagerDidUpdate, object: self)
	}
	
	func loadDataFromUserDefaultsIfNecessary(currentLocation: CLLocation) {
		
		if (forceReset) {
			attemptedLoadFromUserDefaults = true
			return
		}
		
		if (!attemptedLoadFromUserDefaults) {
			attemptedLoadFromUserDefaults = true
			
			let sunData = SunData.loadFromUserDefaults()
			if let sunData = sunData {
				if !sunData.inAcceptableRange(of: currentLocation) {
					return
				}
				
				if sunData.isOutOfDate {
					return
				}

				currentSunData = sunData
				completeInitiation(fromCache:true)
			}
		}
	}
	
	func saveDataToUserDefaults() {
		currentSunData!.writeToUserDefaults()
	}
	
	func fetchSolarTimes( for day: RelativeDay, sunData: SunData, completionHandler: @escaping (SunData, ErrorType) -> Void ) {
		
		let dataHandler : DataHandler = SRSSOrgDataHandler()
		
		let url = dataHandler.urlForRequest(for: day, sunData: sunData)
		
		let session = URLSession.shared
		let dataTask = session.dataTask(with: url!) { (data: Data?, response: URLResponse?, error: Error?) in
			if (data == nil) {
				print("Failuring fetching data from service")
				DispatchQueue.main.async {
					completionHandler(sunData,.timeServiceFailure)
				}
				return
			}
			
			let processedResponseData = dataHandler.processResponseData(data!, day: day)
			
			var sunData = sunData
			if let yesterday = processedResponseData.yesterday {
				sunData.yesterday = yesterday
			}
			
			if let today = processedResponseData.today {
				sunData.today = today
			}
			
			if let tomorrow = processedResponseData.tomorrow {
				sunData.tomorrow = tomorrow
			}
			
			if sunData.isComplete {
				/*if !sunData.hasTwilightData {
					sunData.interpolateTwilightData()
				}*/
				
				if !sunData.isCoherent {
					print("Solar phenomenon sequences out of order")
					DispatchQueue.main.async {
						completionHandler(sunData, .timeServiceFailure)
					}
					return
				}
			}
			DispatchQueue.main.async {
				completionHandler(sunData, .noError)
			}
		}
		
		dataTask.resume()
	}
	
	func overrideTimeToBeforeSunset() {
		assert(currentSunData!.isComplete)
		
		timeOffsetOverride = -Date().timeIntervalSince(currentSunData!.today!.sunset) - 5.0 //5 seconds before sunset
	}
}

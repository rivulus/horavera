//
//  HourDial.swift
//  horavera
//
//  Created by Philip White on 11/15/18.
//  Copyright © 2018 Philip White. All rights reserved.
//

import UIKit

extension UIColor {
	convenience init(r: Int, g: Int, b: Int) {
		self.init(red: CGFloat(r)/255.0, green: CGFloat(g)/255.0, blue: CGFloat(b)/255.0, alpha: 1.0)
	}
}

public class HourDialLayer: CALayer {
	
	@NSManaged public dynamic var totalDaytimeAngle : CGFloat
	
	// The angle the clock needs to be rotated from dawn to be at the correct hour
	@NSManaged public dynamic var currentTimeAngle : CGFloat
	
	// The angle before dawn at which the background begins to lighten (absolute value)
	@NSManaged public dynamic var morningTwilightAngle : CGFloat
	// The angle after sunset at which the background finishes darkening (absolute value)
	@NSManaged public dynamic var eveningTwilightAngle : CGFloat
	
	@NSManaged public dynamic var showCross : Bool
	@NSManaged public dynamic var showDayHours : Bool
	@NSManaged public dynamic var showDayWatches : Bool
	@NSManaged public dynamic var showNightHours : Bool
	@NSManaged public dynamic var showNightWatches : Bool
	
	// Variables used to generate icon images
	public var showText : Bool = true
	public var showPeriods : Bool = true
	
	override public class func defaultValue(forKey key: String) -> Any? {
		let angleKeys = [#keyPath(totalDaytimeAngle), #keyPath(currentTimeAngle),
					   #keyPath(morningTwilightAngle), #keyPath(eveningTwilightAngle)]
		if angleKeys.contains(key) {
			return 0.0
		} else if key == #keyPath(showCross) {
			return true
		} else if key == #keyPath(showDayHours) {
			return true
		} else if key == #keyPath(showDayWatches) {
			return false
		} else if key == #keyPath(showNightHours) {
			return true
		} else if key == #keyPath(showNightWatches) {
			return true
		}else {
			return super.defaultValue(forKey: key)
		}
	}
	
	override public class func needsDisplay(forKey key: String) -> Bool {
		let allKeys = [#keyPath(totalDaytimeAngle), #keyPath(currentTimeAngle),
					   #keyPath(morningTwilightAngle), #keyPath(eveningTwilightAngle), #keyPath(showCross), #keyPath(showDayHours), #keyPath(showDayWatches), #keyPath(showNightHours), #keyPath(showNightWatches)]
		
		if allKeys.contains(key) {
			return true
		}
		return super.needsDisplay(forKey: key)
	}
	
	var pressedPeriod : HourDialPeriod = .null {
		didSet {
			if pressedPeriod != oldValue {
				self.setNeedsDisplay()
			}
		}
	}
	
	private var totalNighttimeAngle : CGFloat {
		return 2.0 * CGFloat.pi - totalDaytimeAngle
	}
	
	private var daytimeHourAngle : CGFloat {
		return (totalDaytimeAngle / 12.0)
	}
	
	private var nighttimeHourAngle : CGFloat {
		return (totalNighttimeAngle / 12.0)
	}
	
	private var showNightHourText : Bool {
		return showText && totalNighttimeAngle/(2 * .pi) >= 0.15
	}
	
	private var showDayHourText : Bool  {
		return showText && totalDaytimeAngle/(2 * .pi) >= 0.15
	}
	
	private var showVigilText : Bool  {
		return showText
	}
	
	private var showDayWatchText : Bool  {
		return showText
	}
	
	private var showSingleNightSection : Bool {
		return !(showPeriods && totalNighttimeAngle / (2.0 * .pi) >= 0.115)
	}
	
	private var showSingleDaySection : Bool {
		return !(showPeriods && totalDaytimeAngle / (2.0 * .pi) >= 0.115)
	}
	
	var cachedNighttimeHourPaths : [UIBezierPath]! = nil
	var cachedVigilPaths : [UIBezierPath]! = nil
	var cachedDaytimeHourPaths : [UIBezierPath]! = nil
	var cachedDayWatchPaths : [UIBezierPath]! = nil
	
	private static let darkNighttimeColor = UIColor(r:88, g:86, b:214)
	private static let lightDaytimeColor = UIColor(r:90, g:200, b:250)
	private static let daytimeColor = UIColor(r:255, g:204, b:0)
	private static let nighttimeColor = UIColor(r:0, g:122, b:255)
	private static let pointerColor = UIColor(r:142, g:142, b:147)
	private static let separatorWidth : CGFloat = 6.0
	
	lazy var backgroundGradient : CGGradient! = {
		let colorSpace = CGColorSpaceCreateDeviceRGB()
		let orangeColor = UIColor(r:255, g:149, b:0)
		
		let colors = [HourDialLayer.darkNighttimeColor.cgColor,
					  orangeColor.cgColor,
					  orangeColor.cgColor,
					  HourDialLayer.lightDaytimeColor.cgColor,]
		return CGGradient(colorsSpace:colorSpace, colors:colors as CFArray, locations:[0,0.48,0.52,1])
	}()
	// MARK: Methods
	override public func draw(in ctx: CGContext) {
		
		super.draw(in: ctx)
		
		UIGraphicsPushContext(ctx)
		
		drawBackground(in: ctx)
		
		let pointerPath = dialPointerPath()
		
		HourDialLayer.pointerColor.setFill()
		pointerPath.fill()
		
		if totalDaytimeAngle != 0.0 {
			drawClockFace(in: ctx)
		}
		
		UIGraphicsPopContext()
	}
	
	func drawBackground(in context: CGContext) {
		let height = bounds.maxY
		
		var percentComplete : CGFloat = 0
		var isSunset = false
		var isSunrise = false
		
		if currentTimeAngle < eveningTwilightAngle { // after sunset
			percentComplete = 0.5 + 0.5 * currentTimeAngle/eveningTwilightAngle
			isSunset = true
		} else if currentTimeAngle > 2 * .pi - eveningTwilightAngle { // before sunset
			percentComplete = 0.5 * (1.0 - (2 * .pi - currentTimeAngle) / eveningTwilightAngle)
			isSunset = true
		} else if currentTimeAngle > totalNighttimeAngle - morningTwilightAngle &&
			currentTimeAngle <= totalNighttimeAngle { // before sunrise
			percentComplete = 0.5 * (1.0 - (totalNighttimeAngle - currentTimeAngle) / morningTwilightAngle)
			isSunrise = true
		} else if currentTimeAngle > totalNighttimeAngle &&
			currentTimeAngle < totalNighttimeAngle + morningTwilightAngle { // after sunrise
			percentComplete = 0.5 + 0.5 * ((currentTimeAngle - totalNighttimeAngle) / morningTwilightAngle)
			isSunrise = true
		}
		
		if (isSunset || isSunrise) {
			var startPoint = CGPoint(x:0, y:3.0 * height)
			var endPoint = CGPoint(x:0, y:1.0 * height)
			var yShift : CGFloat = 3.0 * height * percentComplete
			
			if (isSunset) { // Gradient come down from top of screen (darkness falls)
				startPoint = CGPoint(x:0, y:-2.0 * height)
				endPoint = CGPoint(x:0, y:0.0 * height)
			} else if (isSunrise) { // Gradient comes up from bottom of screen
				startPoint = CGPoint(x:0, y:1.0 * height)
				endPoint = CGPoint(x:0, y:3.0 * height)
				yShift = -yShift
			}
			
			startPoint.y += yShift
			endPoint.y += yShift
			context.drawLinearGradient(backgroundGradient,
									   start: startPoint,
									   end: endPoint,
									   options: [CGGradientDrawingOptions.drawsBeforeStartLocation, CGGradientDrawingOptions.drawsAfterEndLocation])
		} else {
			if currentTimeAngle < totalNighttimeAngle {
				HourDialLayer.darkNighttimeColor.setFill()
				let backgroundPath = UIBezierPath(rect: bounds)
				backgroundPath.fill()
			} else {
				HourDialLayer.lightDaytimeColor.setFill()
				let backgroundPath = UIBezierPath(rect: bounds)
				backgroundPath.fill()
			}
		}
	}
	
	func drawClockFace(in context: CGContext) {
		if showSingleNightSection {
			HourDialLayer.nighttimeColor.setFill()
			let nighttimePath = singleNighttimePath()
			nighttimePath.fill()
		} else {
			cachedNighttimeHourPaths = nil
			if showNightHours {
				HourDialLayer.nighttimeColor.setFill()
				cachedNighttimeHourPaths = nighttimeHourPaths()
				for (hour, nighttimeSegmentPath) in cachedNighttimeHourPaths.enumerated() {
					nighttimeSegmentPath.lineWidth = HourDialLayer.separatorWidth
					nighttimeSegmentPath.fill()
					
					if pressedPeriod == .night(hour) {
						highlightTouchedSegment(nighttimeSegmentPath, oldFillColor: HourDialLayer.nighttimeColor)
					}
				}
				
				if showNightHourText {
					UIColor.black.setFill()
					for textPath in nighttimeTextPaths() {
						textPath.fill()
					}
				}
			}
			
			cachedVigilPaths = nil
			if showNightWatches {
				HourDialLayer.nighttimeColor.setFill()
				cachedVigilPaths = vigilPaths()
				for (vigil, vigilPath) in cachedVigilPaths.enumerated() {
					vigilPath.lineWidth = HourDialLayer.separatorWidth

					vigilPath.fill()
					
					if pressedPeriod == .vigil(vigil) {
						highlightTouchedSegment(vigilPath, oldFillColor: HourDialLayer.nighttimeColor)
					}
				}
				
				if showVigilText {
					for vigilTextPath in vigilTextPaths() {
						UIColor.black.setFill()
						vigilTextPath.fill()
					}
				}
			}
		}
		
		HourDialLayer.daytimeColor.setFill()
		
		if showSingleDaySection {
			let daytimePath = singleDaytimePath()
			daytimePath.fill()
		} else {
			cachedDaytimeHourPaths = nil
			if showDayHours {
				cachedDaytimeHourPaths = daytimeHourPaths()
				for (hour, dayTimeSegmentPath) in cachedDaytimeHourPaths.enumerated() {
					dayTimeSegmentPath.lineWidth = HourDialLayer.separatorWidth

					dayTimeSegmentPath.fill()
					
					if pressedPeriod == .day(hour) {
						highlightTouchedSegment(dayTimeSegmentPath, oldFillColor: HourDialLayer.daytimeColor)
					}
				}
				
				if showDayHourText {
					for textPath in daytimeTextPaths() {
						UIColor.black.setFill()
						textPath.fill()
					}
				}
			}
			
			if showDayWatches {
				HourDialLayer.daytimeColor.setFill()
				cachedDayWatchPaths = dayWatchPaths()
				for watchPath in cachedDayWatchPaths {
					watchPath.lineWidth = HourDialLayer.separatorWidth
					watchPath.fill()
				}
				
				if showDayWatchText {
					for watchTextPath in dayWatchTextPaths() {
						UIColor.black.setFill()
						watchTextPath.fill()
					}
				}
			}
		}
		
		if (showCross) {
			let crossPath = centerCross()
			let transformedCrossPath = transformPath(crossPath, rotate: false)
			HourDialLayer.pointerColor.setFill()
			transformedCrossPath.fill()
		}
	}
	
	func highlightTouchedSegment(_ path : UIBezierPath, oldFillColor : UIColor) {
		pressedOverlayColor().setFill()
		path.fill()
		oldFillColor.setFill()
	}
	
	func pressedOverlayColor() -> UIColor {
		return UIColor(white: 0.0, alpha: 0.2)
	}
	
	func transformPath(_ path: UIBezierPath, rotate: Bool) -> UIBezierPath {
		let transformedPath = path.copy() as! UIBezierPath
		
		if (rotate) {
			// Rotate to the correct time
			let additionalAngleToPointer = CGFloat.pi / 2.0
			transformedPath.apply(CGAffineTransform(rotationAngle: -(additionalAngleToPointer + currentTimeAngle)))
		}
		// Scale and transform to the size of the dial
		let rect = dialRect()
		transformedPath.apply(CGAffineTransform(scaleX: rect.size.width/2.0, y: rect.size.height/2.0))
		transformedPath.apply(CGAffineTransform(translationX: rect.midX, y: rect.midY))
		
		return transformedPath
	}
	
	// MARK: Hours and Watches
	
	func singleNighttimePath() -> UIBezierPath {
		let path = segmentPathForAngle(totalNighttimeAngle, outerRadius: 1.0, innerRadius: 0.25, clockwise: true)
		return transformPath(path, rotate: true)
	}
	
	func singleDaytimePath() -> UIBezierPath {
		let path = segmentPathForAngle(totalDaytimeAngle, outerRadius: 1.0, innerRadius: 0.25, clockwise: false)
		return transformPath(path, rotate: true)
	}
	
	func nighttimeHourPaths() -> [UIBezierPath] {
		var paths = [UIBezierPath]()
		var path : UIBezierPath!
		
		if showNightWatches {
			// If showing nightwatches (vigils) we need to leave a gap for those
			path = segmentPathForAngle(nighttimeHourAngle, outerRadius: 1.0, innerRadius: 0.75, clockwise: true)
			path.append(segmentPathForAngle(nighttimeHourAngle, outerRadius: 0.60, innerRadius: 0.25, clockwise: true))
		} else {
			path = segmentPathForAngle(nighttimeHourAngle, outerRadius: 1.0, innerRadius: 0.25, clockwise: true)
		}
		
		paths.append(transformPath(path, rotate: true))
		for angleMultiplier in 1...11 {
			let rotatedPath = path.copy() as! UIBezierPath
			rotatedPath.apply(CGAffineTransform(rotationAngle: CGFloat(angleMultiplier) * nighttimeHourAngle))
			paths.append(transformPath(rotatedPath, rotate: true))
		}
		return paths
	}
	
	func vigilPaths() -> [UIBezierPath] {
		var paths = [UIBezierPath]()
		
		let vigilAngle = nighttimeHourAngle*3.0
		var path : UIBezierPath!
		
		if showNightHours {
			// If showing night hours, we make the night watches (vigils) small, to fit
			// among the hours
			path = segmentPathForAngle(vigilAngle, outerRadius: 0.75, innerRadius: 0.6, clockwise: true)
		} else {
			path = segmentPathForAngle(vigilAngle, outerRadius: 1.0, innerRadius: 0.25, clockwise: true)
		}
		
		paths.append(transformPath(path, rotate: true))
		for angleMultiplier in 1...3 {
			let rotatedPath = path.copy() as! UIBezierPath
			rotatedPath.apply(CGAffineTransform(rotationAngle: CGFloat(angleMultiplier) * vigilAngle))
			paths.append(transformPath(rotatedPath, rotate: true))
		}
		return paths
	}
	
	func daytimeHourPaths() -> [UIBezierPath] {
		var paths = [UIBezierPath]()
		var path : UIBezierPath!
		
		if showDayWatches {
			// If showing daywatches we need to leave a gap for those
			path = segmentPathForAngle(daytimeHourAngle, outerRadius: 1.0, innerRadius: 0.75, clockwise: false)
			path.append(segmentPathForAngle(daytimeHourAngle, outerRadius: 0.60, innerRadius: 0.25, clockwise: false))
		} else {
			path = segmentPathForAngle(daytimeHourAngle, outerRadius: 1.0, innerRadius: 0.25, clockwise: false)
		}
		
		paths.append(transformPath(path, rotate: true))
		for angleMultiplier in 1...11 {
			let rotatedPath = path.copy() as! UIBezierPath
			rotatedPath.apply(CGAffineTransform(rotationAngle: -1.0 * CGFloat(angleMultiplier) * daytimeHourAngle))
			paths.append(transformPath(rotatedPath, rotate: true))
		}
		return paths
	}
	
	func dayWatchPaths() -> [UIBezierPath] {
		var paths = [UIBezierPath]()
		
		let watchAngle = daytimeHourAngle*3.0
		var path : UIBezierPath!
		
		if showDayHours {
			// If showing day hours, we make the day watches small, to fit
			// among the hours
			path = segmentPathForAngle(watchAngle, outerRadius: 0.75, innerRadius: 0.6, clockwise: true)
		} else {
			path = segmentPathForAngle(watchAngle, outerRadius: 1.0, innerRadius: 0.25, clockwise: true)
		}
		
		for angleMultiplier in 1...4 {
			let rotatedPath = path.copy() as! UIBezierPath
			rotatedPath.apply(CGAffineTransform(rotationAngle: -1.0 * CGFloat(angleMultiplier) * watchAngle))
			paths.append(transformPath(rotatedPath, rotate: true))
		}
		return paths
	}
	
	func segmentPathForAngle(_ angle : CGFloat, outerRadius rawOuterRadius : CGFloat, innerRadius rawInnerRadius : CGFloat, clockwise : Bool ) -> UIBezierPath {
		let path = UIBezierPath()
		var actualAngle : CGFloat = 0.0
		
		let separatorGap : CGFloat = 0.01
		let innerRadius = rawInnerRadius + separatorGap
		let outerRadius = rawOuterRadius - separatorGap
		
		var innerGapAngle = angleForChord(length: separatorGap, radius: innerRadius)
		var outerGapAngle = angleForChord(length: separatorGap, radius: outerRadius)
		
		if clockwise {
			actualAngle = angle
		} else {
			actualAngle = -angle
			innerGapAngle = -innerGapAngle
			outerGapAngle = -outerGapAngle
		}
		
		path.move(to: polarToCartesian(angle: innerGapAngle, radius: innerRadius))
		path.addLine(to: polarToCartesian(angle: outerGapAngle, radius: outerRadius))
		path.addArc(withCenter: CGPoint(x:0.0, y:0.0), radius: outerRadius, startAngle: outerGapAngle, endAngle: actualAngle-outerGapAngle, clockwise: clockwise)
		
		if innerRadius > 0.0 && abs(actualAngle) > 2 * abs(innerGapAngle) {
			path.addLine(to: polarToCartesian(angle: actualAngle-innerGapAngle, radius: innerRadius))
				
			path.addArc(withCenter: CGPoint(x:0.0, y:0.0), radius: innerRadius, startAngle: actualAngle-innerGapAngle, endAngle: innerGapAngle, clockwise: !clockwise)
		}
		path.close()
		
		return path
	}
	
	//MARK: Ornamentation
	
	func centerCross() -> UIBezierPath {
		let quarterCircle : CGFloat = 2.0 * CGFloat.pi / 4.0
		let eighthCircle : CGFloat = 2.0 * CGFloat.pi / 8.0
		let overallRadius : CGFloat = 0.23
		let centerRadius : CGFloat = 0.06
		let armAngle : CGFloat = 2.0 * CGFloat.pi / 4.8
		let controlPointRadius : CGFloat = 0.145
		let controlPointAngleOffset = eighthCircle/4.0
		
		let path = UIBezierPath()
		
		path.move(to: polarToCartesian(angle: -eighthCircle, radius: centerRadius))
		
		for angleMultiplier in 0...3 {
			let angleRotation = quarterCircle * CGFloat(angleMultiplier)
			
			let controlPoint1 = polarToCartesian(angle: angleRotation-controlPointAngleOffset, radius: controlPointRadius)
			let controlPoint2 = polarToCartesian(angle: angleRotation+controlPointAngleOffset, radius: controlPointRadius)
			
			path.addQuadCurve(to: polarToCartesian(angle: -armAngle/2.0+angleRotation, radius: overallRadius), controlPoint: controlPoint1)
			path.addArc(withCenter: CGPoint(x: 0.0, y: 0.0), radius: overallRadius, startAngle: -armAngle/2.0 + angleRotation, endAngle: armAngle/2.0+angleRotation, clockwise: true)
			path.addQuadCurve(to: polarToCartesian(angle: eighthCircle+angleRotation, radius: centerRadius), controlPoint: controlPoint2)
		}
		
		path.close()
		
		return path
	}
	
	func dialPointerPath() -> UIBezierPath {
		let pointerAspectRatio : CGFloat = 5.0/4.0 //height over width
		let pointerGapRatio : CGFloat = 0.01// gap between dial and pointer as fraction of the height of pointer and gap
		
		let dialRect = self.dialRect()
		let dialPlusPointerRect = self.dialPlusPointerRect()
		let pointerPlusGapHeight = dialPlusPointerRect.height - dialRect.height
		
		let pointerGap = pointerPlusGapHeight * pointerGapRatio // gap between the dial and the pointer
		let pointerHeight = pointerPlusGapHeight - pointerGap
		let pointerWidth = pointerHeight / pointerAspectRatio
		
		let dialOrigin = CGPoint(x:dialRect.midX, y:dialRect.midY)
		let dialRadius = dialRect.size.width / 2.0
		
		// Draw the pointer
		let pointerPath = UIBezierPath()
		var currentPoint = CGPoint(x: dialOrigin.x, y: dialOrigin.y - dialRadius - pointerGap)
		pointerPath.move(to: currentPoint)
		let tipOfPointer = currentPoint
		
		var controlPoint = tipOfPointer;
		controlPoint.y -= pointerHeight / 2.0;
		
		currentPoint.y -= pointerHeight
		currentPoint.x += pointerWidth / 2.0
		pointerPath.addQuadCurve(to: currentPoint, controlPoint: controlPoint)
		
		let overallHeight = dialRadius + pointerGap + pointerHeight
		
		let topArcRadius = (overallHeight*overallHeight + pointerWidth*pointerWidth/4.0).squareRoot()
		let topArcHalfAngle = atan(pointerWidth/(2.0*overallHeight))
		
		let topArcStartAngle = 3.0*CGFloat.pi/2.0 - topArcHalfAngle
		let topArcEndAngle = 3.0*CGFloat.pi/2.0 + topArcHalfAngle
		
		currentPoint.x -= pointerWidth
		
		pointerPath.addArc(withCenter: dialOrigin, radius: topArcRadius, startAngle: topArcEndAngle, endAngle:topArcStartAngle, clockwise: false)
		
		pointerPath.addQuadCurve(to: tipOfPointer, controlPoint: controlPoint)
		
		return pointerPath
	}
	
	func dialPlusPointerRect() -> CGRect {
		let dialInset = (bounds.size.width/40.0).rounded(.up) // gap between the dial and sides of the screens
		let aspectRatio : CGFloat = 1.125 // Height over width, extra height is for pointer)
		var squareRect = CGRect.zero
		
		if bounds.size.height >= aspectRatio * (bounds.size.width - 2.0 * dialInset) + 2 * dialInset {
			squareRect.size.width = bounds.size.width - 2.0 * dialInset
			squareRect.size.height = squareRect.width * aspectRatio
		} else {
			squareRect.size.height = bounds.size.height - 2.0 * dialInset
			squareRect.size.width = squareRect.size.height / aspectRatio
		}
		
		squareRect.origin.x = bounds.origin.x + (bounds.size.width - squareRect.size.width) / 2.0
		
		squareRect.origin.y = bounds.origin.y + (bounds.size.height - squareRect.size.height) / 2.0
		
		return squareRect
	}
	
	func dialRect() -> CGRect {
		var dialRect = dialPlusPointerRect()
		dialRect.origin.y += dialRect.size.height - dialRect.size.width
		dialRect.size.height = dialRect.size.width
		
		return dialRect
	}
	
	//MARK: Labels
	
	func nighttimeTextPaths() -> [UIBezierPath] {
		var nighttimeTextPaths = [UIBezierPath]()
		
		for hour in 0...11 {
			let path = pathForHourLabel(.night(hour))
			nighttimeTextPaths.append(transformPath(path, rotate: true))
		}
		return nighttimeTextPaths
	}
	
	func daytimeTextPaths() -> [UIBezierPath] {
		var daytimeTextPaths = [UIBezierPath]()
		
		for hour in 0...11 {
			let path = pathForHourLabel(.day(hour))
			daytimeTextPaths.append(transformPath(path, rotate: true))
		}
		return daytimeTextPaths
	}
	
	func vigilTextPaths() -> [UIBezierPath] {
		var vigilTextPaths = [UIBezierPath]()
		
		for vigilOrdinal in 0...3 {
			let path = pathForVigilText(vigilOrdinal)
			vigilTextPaths.append(transformPath(path, rotate: true))
		}
		
		return vigilTextPaths
	}
	
	func dayWatchTextPaths() -> [UIBezierPath] {
		var watchTextPaths = [UIBezierPath]()
		
		for watchOrdinal in 0...3 {
			let path = pathForDayWatchText(watchOrdinal)
			watchTextPaths.append(transformPath(path, rotate: true))
		}
		
		return watchTextPaths
	}
	
	func vigilStringForInteger(_ integer : Int) -> String {
		switch integer {
		case 0:
			return "Prima"
		case 1:
			return "Secunda"
		case 2:
			return "Tertia"
		case 3:
			return "Quarta"
		default:
			print("Error")
			return "Error"
		}
	}
	
	func pathForHourLabel(_ period : HourDialPeriod) -> UIBezierPath {
		var fontSize : CGFloat = 0.1
		var hour : Int = 0
		var rotationAngle : CGFloat = 0.0
		
		switch period {
		case .day(let h):
			hour = h
			rotationAngle = -((daytimeHourAngle / 2.0) + (CGFloat(12 - (hour + 1)) * daytimeHourAngle))
			fontSize = dayHourFontSize()
			break
		case .night(let h):
			hour = h
			rotationAngle = (nighttimeHourAngle / 2.0) + (CGFloat(hour) * nighttimeHourAngle)
			fontSize = nightHourFontSize()
			break
		default:
			break
		}
		
		let string = romanNumeralForInteger(hour + 1)
		
		#if true //Curve the roman numerals
		let path = pathForString(string, fontSize: fontSize, curve: true, curveRadius: 0.875)
		#else
		let path = pathForString(string, fontSize: fontSize, curve: false, curveRadius: 0)
		path.apply(CGAffineTransform(translationX: 0.0, y: -0.85))
		path.apply(CGAffineTransform(translationX: -path.bounds.size.width/2.0, y: 0.0))
		#endif
		
		path.apply(CGAffineTransform(rotationAngle: (CGFloat.pi / 2.0) + rotationAngle))
		
		return path
	}
	
	func pathForVigilText(_ integer: Int ) -> UIBezierPath {
		let fontSize : CGFloat = vigilFontSize()
		let string = vigilStringForInteger(integer)
		let curveRadius : CGFloat = showNightHours ? 0.675 : 0.875
		let path = pathForString(string, fontSize: fontSize, curve:true, curveRadius: curveRadius)
		
		let vigilAngle = nighttimeHourAngle * 3.0
		path.apply(CGAffineTransform(rotationAngle: (CGFloat.pi / 2.0)))
		
		path.apply(CGAffineTransform(rotationAngle: (vigilAngle / 2.0) + (CGFloat(integer) * vigilAngle)))
		
		return path
	}
	
	func pathForDayWatchText(_ integer: Int ) -> UIBezierPath {
		let fontSize : CGFloat = dayWatchFontSize()
		let string = vigilStringForInteger(integer)
		let curveRadius : CGFloat = showDayHours ? 0.675 : 0.875
		let path = pathForString(string, fontSize: fontSize, curve:true, curveRadius: curveRadius)
		
		let dayWatchAngle = daytimeHourAngle * 3.0
		path.apply(CGAffineTransform(rotationAngle: (CGFloat.pi / 2.0)))
		
		path.apply(CGAffineTransform(rotationAngle: -1.0 * dayWatchAngle / 2.0 - (CGFloat(4 - (integer + 1)) * dayWatchAngle)))
		
		return path
	}
	
	func pathForString(_ string: String, fontSize: CGFloat, curve: Bool, curveRadius rawCurveRadius: CGFloat) -> UIBezierPath {
		let fontName: CFString = "Baskerville" as CFString
		
		let letters = CGMutablePath()
		let font = CTFontCreateWithName(fontName, fontSize, nil)
		let capHeight = renderedCapHeight(font)
		let curveRadius = rawCurveRadius - capHeight/2.0
		let attrString = NSAttributedString(string: string, attributes: [kCTFontAttributeName as NSAttributedString.Key : font])
		let line = CTLineCreateWithAttributedString(attrString)
		let runArray = CTLineGetGlyphRuns(line)
		
		let path = UIBezierPath()
		
		guard CFArrayGetCount(runArray) == 1 else {
			print("Error rendering text, more than one glyph run in string")
			return UIBezierPath()
		}
		
		let run : CTRun = unsafeBitCast(CFArrayGetValueAtIndex(runArray, 0), to: CTRun.self)
		let dictRef : CFDictionary = CTRunGetAttributes(run)
		let dict : NSDictionary = dictRef as NSDictionary
		let runFont = dict[kCTFontAttributeName as String] as! CTFont
		
		let imageBounds = CTRunGetImageBounds(run, nil, CFRange.init())
		
		for runGlyphIndex in 0..<CTRunGetGlyphCount(run) {
			let thisGlyphRange = CFRangeMake(runGlyphIndex, 1)
			var glyph = CGGlyph()
			var position = CGPoint.zero
			CTRunGetGlyphs(run, thisGlyphRange, &glyph)
			CTRunGetPositions(run, thisGlyphRange, &position)
			let length = CGFloat(CTRunGetTypographicBounds(run, thisGlyphRange, nil, nil, nil))
			
			let letter = CTFontCreatePathForGlyph(runFont, glyph, nil)
			
			var t : CGAffineTransform! = nil
			
			if (curve) {
				let angle = (position.x + CGFloat(length)/2.0 - imageBounds.size.width/2.0) / curveRadius
				let t1 = CGAffineTransform(translationX: -length/2.0, y: curveRadius)
				let t2 = CGAffineTransform(rotationAngle: -angle)
				t = t1.concatenating(t2)
			} else {
				t = CGAffineTransform(translationX: position.x, y: position.y)
			}
			if let letter = letter  {
				letters.addPath(letter, transform: t)
			}
		}
		path.move(to: CGPoint.zero)
		path.append(UIBezierPath(cgPath: letters))
		
		path.apply(CGAffineTransform(scaleX: 1.0, y: -1.0))
		
		return path
	}
	
	func renderedCapHeight(_ font: CTFont) -> CGFloat {
		let attrString = NSAttributedString(string: "X", attributes: [kCTFontAttributeName as NSAttributedString.Key : font])
		let line = CTLineCreateWithAttributedString(attrString)
		let runArray = CTLineGetGlyphRuns(line)
		
		let run : CTRun = unsafeBitCast(CFArrayGetValueAtIndex(runArray, 0), to: CTRun.self)
		let imageBounds = CTRunGetImageBounds(run, nil, CFRange.init())
		return imageBounds.height
	}
	
	func romanNumeralForInteger(_ integer: Int) -> String {
		switch integer {
		case 1:
			return "I"
		case 2:
			return "II"
		case 3:
			return "III"
		case 4:
			return "IV"
		case 5:
			return "V"
		case 6:
			return "VI"
		case 7:
			return "VII"
		case 8:
			return "VIII"
		case 9:
			return "IX"
		case 10:
			return "X"
		case 11:
			return "XI"
		case 12:
			return "XII"
		default:
			print("Error")
			return "OO"
		}
	}
	
	func nightHourFontSize() -> CGFloat {
		var fontSize : CGFloat = 0.09
		let cutoff : CGFloat = 0.48
		let nighttimeRatio : CGFloat = totalNighttimeAngle / (2.0 * .pi)
		
		if nighttimeRatio < cutoff {
			let multiplier = nighttimeRatio / cutoff
			fontSize *= multiplier
		}
		
		return fontSize
	}
	
	func dayHourFontSize() -> CGFloat {
		var fontSize : CGFloat = 0.09
		let cutoff : CGFloat = 0.48
		let daytimeRatio : CGFloat = totalDaytimeAngle / (2.0 * .pi)
		
		if daytimeRatio < cutoff {
			let multiplier = daytimeRatio / cutoff
			fontSize *= multiplier
		}
		
		return fontSize
	}
	
	func vigilFontSize() -> CGFloat {
		var fontSize : CGFloat = 0.09
		let cutoff : CGFloat = 0.39
		let nighttimeRatio : CGFloat = totalNighttimeAngle / (2.0 * .pi)
		
		if nighttimeRatio < cutoff {
			let multiplier = nighttimeRatio / cutoff
			fontSize *= multiplier
		}
		
		return fontSize
	}
	
	func dayWatchFontSize() -> CGFloat {
		var fontSize : CGFloat = 0.09
		let cutoff : CGFloat = 0.39
		let daytimeRatio : CGFloat = totalDaytimeAngle / (2.0 * .pi)
		
		if daytimeRatio < cutoff {
			let multiplier = daytimeRatio / cutoff
			fontSize *= multiplier
		}
		
		return fontSize
	}
	
	// MARK: Misc.
	func angleForChord(length: CGFloat, radius: CGFloat) -> CGFloat {
		return 2*asin(length/(2.0*radius))
	}
	
	func polarToCartesian(angle : CGFloat, radius : CGFloat ) -> CGPoint {
		let x = cos(angle) * radius
		let y = sin(angle) * radius
		
		return CGPoint(x:x, y:y)
	}
}

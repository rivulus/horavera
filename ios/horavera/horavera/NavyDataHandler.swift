//
//  NavyData.swift
//  horavera
//
//  Created by Philip White on 11/27/18.
//  Copyright © 2018 Philip White. All rights reserved.
//

import Foundation
import CoreLocation

// Request:
// http://api.usno.navy.mil/rstt/oneday?date=11/11/2018&coords=47.605N,121.9004W&tz=-8
// Response:
//{
//	"error":false,
//	"apiversion":"2.2.0",
//	"year":2018,
//	"month":11,
//	"day":11,
//	"dayofweek":"Sunday",
//	"datechanged":false,
//	"lon":-121.900400,
//	"lat":47.605000,
//	"tz":-8,
//
//	"sundata":[
//	{"phen":"BC", "time":"06:34"},
//	{"phen":"R", "time":"07:07"},
//	{"phen":"U", "time":"11:52"},
//	{"phen":"S", "time":"16:36"},
//	{"phen":"EC", "time":"17:09"}],
//
//	"moondata":[
//	{"phen":"R", "time":"10:59"},
//	{"phen":"U", "time":"15:26"},
//	{"phen":"S", "time":"19:53"}],
//
//	"closestphase":{"phase":"First Quarter","date":"November 15, 2018","time":"06:54"},
//	"fracillum":"17%",
//	"curphase":"Waxing Crescent"
//}

struct NavyData : Codable {
	var sundata : [NavySunData]
}

struct NavySunData : Codable {
	var phen: String
	var time: String
}

class NavyDataHandler : DataHandler {
	func urlForRequest(for day: RelativeDay, sunData: SunData) -> URL! {
		var urlString = "http://api.usno.navy.mil/rstt/oneday?"
		urlString += dateQueryComponent(for: day);
		let coordinate = sunData.location.coordinate
		urlString += "&" + coordinatesQueryComponent(latitude: coordinate.latitude, longitude: coordinate.longitude)
		urlString += "&" + timeZoneQueryComponent()
		
		return URL(string: urlString)
	}
	
	func processResponseData(_ data: Data, day: RelativeDay )  -> ProcessedResponseData {
		var yesterdaySunsetTime, todaySunriseTime, todaySunsetTime, tomorrowSunriseTime : Date!
		
		do {
			let decoder = JSONDecoder()
			let navyData = try decoder.decode(NavyData.self, from: data)
			
			for phenomenon in navyData.sundata {
				switch day {
				case .yesterday:
					if phenomenon.phen == "S" {
						yesterdaySunsetTime = dateFrom24HourTimeString(time: phenomenon.time, for: day)
					}
				case .today:
					if phenomenon.phen == "R" {
						todaySunriseTime = dateFrom24HourTimeString(time: phenomenon.time, for: day)
					} else if phenomenon.phen == "S" {
						todaySunsetTime = dateFrom24HourTimeString(time: phenomenon.time, for: day)
					}
				case .tomorrow:
					if phenomenon.phen == "R" {
						tomorrowSunriseTime = dateFrom24HourTimeString(time: phenomenon.time, for: day)
					}
				}
			}
		} catch {
			print("Catch JSON Error")
			print(error)
		}
		
		return ProcessedResponseData(yesterdaySunsetTime: yesterdaySunsetTime,
									 yesterdayEveningTwilightTime: nil,
									 todaySunriseTime: todaySunriseTime,
									 todayMorningTwilightTime: nil,
									 todaySunsetTime: todaySunsetTime,
									 todayEveningTwilightTime: nil,
									 tomorrowSunriseTime: tomorrowSunriseTime,
									 tomorrowMorningTwilightTime: nil)
	}
	
	func dateQueryComponent(for day: RelativeDay) -> String {
		var component = "date="
		
		let date = roughDate(for: day)
		
		let usLocale = Locale(identifier: "en_US")
		let template = "MM/dd/YYYY"
		let dateFormatter = DateFormatter()
		dateFormatter.locale = usLocale;
		dateFormatter.setLocalizedDateFormatFromTemplate(template);
		
		component += dateFormatter.string(from: date)
		
		return component
	}
	
	func coordinatesQueryComponent(latitude: CLLocationDegrees, longitude: CLLocationDegrees) -> String {
		var component = "coords="
		var latitudeString: String = ""
		var longitudeString: String = ""
		
		if (latitude > 0) {
			latitudeString = String(latitude)+"N"
		} else {
			latitudeString = String(-latitude)+"S"
		}
		
		if (longitude > 0) {
			longitudeString = String(longitude)+"E"
		} else {
			longitudeString = String(-longitude)+"W"
		}
		
		component += latitudeString + "," + longitudeString
		return component
	}
	
	func timeZoneQueryComponent() -> String {
		var component = "tz="
		
		let timeZone = TimeZone.current
		let gmtOffset = timeZone.secondsFromGMT()/(60*60)
		
		component += String(gmtOffset)
		return component
	}
	
	func roughDate(for day: RelativeDay) -> Date {
		var date = Date();
		
		if (day == RelativeDay.yesterday) {
			date = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: date)!
		} else if (day == RelativeDay.tomorrow) {
			date = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: date)!
		}
		
		return date
	}
	
	func dateFrom24HourTimeString(time : String, for day: RelativeDay) -> Date {
		let hourMinute = time.split(separator: ":")
		
		let hour = Int(hourMinute[0])!
		let minute = Int(hourMinute[1])!
		let date = roughDate(for: day)
		var dateComponents = Calendar.current.dateComponents(in: TimeZone.current, from:date)
		dateComponents.hour = hour
		dateComponents.minute = minute
		return dateComponents.date!
	}
}

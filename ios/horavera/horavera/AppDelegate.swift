//
//  AppDelegate.swift
//  horavera
//
//  Created by Philip White on 11/11/18.
//  Copyright © 2018 Philip White. All rights reserved.
//


import UIKit

extension Notification.Name {
	static let clockTick = Notification.Name("clockTick")
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var solarManager: SolarManager?
	var window: UIWindow?
	var timer: Timer?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		Settings().registerDefaults()
		solarManager = SolarManager()
		startTimer()
		return true
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
		startTimer()
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}

	func startTimer() {
		if (timer == nil) {
			timer = Timer(timeInterval: 5.0, repeats: true, block: { (_) in
				NotificationCenter.default.post(name: .clockTick, object: nil)
			})
			RunLoop.current.add(timer!, forMode: RunLoop.Mode.default)
		}
	}
	
	func endTimer() {
		timer?.invalidate()
		timer = nil
	}

	func reset() {
		solarManager = nil
		solarManager = SolarManager()
	}
	
}


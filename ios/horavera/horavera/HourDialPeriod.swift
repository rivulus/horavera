//
//  HourDialPeriod.swift
//  horavera
//
//  Created by Philip White on 2/14/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import Foundation

enum HourDialPeriod : Equatable {
	case null
	case day(Int)
	case night(Int)
	case vigil(Int)
	case prime, terce, sext, none
	case lauds, vespers, compline, matins
	
}

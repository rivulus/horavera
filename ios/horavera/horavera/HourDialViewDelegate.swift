//
//  HourDialViewDelegate.swift
//  horavera
//
//  Created by Philip White on 2/14/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import Foundation

protocol HourDialViewDelegate {
	func hourDialView(_ : HourDialView, didSelectPeriod _: HourDialPeriod)
}

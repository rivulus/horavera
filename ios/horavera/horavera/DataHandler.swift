//
//  DataHandler.swift
//  horavera
//
//  Created by Philip White on 11/27/18.
//  Copyright © 2018 Philip White. All rights reserved.
//

import Foundation
import CoreLocation

struct ProcessedResponseData {
	var yesterday : SunTimes?
	var today : SunTimes?
	var tomorrow : SunTimes?
}

protocol DataHandler {
	func urlForRequest(for day: RelativeDay, sunData: SunData) -> URL!
	
	func processResponseData(_ data: Data, day: RelativeDay ) -> ProcessedResponseData
}

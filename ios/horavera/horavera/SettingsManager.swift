//
//  SettingsManager.swift
//  horavera
//
//  Created by Philip White on 10/16/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

import Foundation

extension Notification.Name {
	static let displaySettingsDidChange = Notification.Name("displaySettingsDidChange")
}

func Settings() -> SettingsManager {
	return SettingsManager.shared
}

class SettingsManager: NSObject {
	fileprivate static var shared: SettingsManager = {
		return SettingsManager()
	}()
	
	var showCenterCross: Bool {
		get {
			return UserDefaults.standard.bool(forKey: "showCenterCross")
		}
		set (newValue) {
			UserDefaults.standard.set(newValue, forKey: "showCenterCross")
			NotificationCenter.default.post(name: .displaySettingsDidChange, object:self)
		}
	}
	
	var showDayHours: Bool {
		get {
			return UserDefaults.standard.bool(forKey: "showDayHours")
		}
		set (newValue) {
			UserDefaults.standard.set(newValue, forKey: "showDayHours")
			NotificationCenter.default.post(name: .displaySettingsDidChange, object:self)
		}
	}
	
	var showDayWatches: Bool {
		get {
			return UserDefaults.standard.bool(forKey: "showDayWatches")
		}
		set (newValue) {
			UserDefaults.standard.set(newValue, forKey: "showDayWatches")
			NotificationCenter.default.post(name: .displaySettingsDidChange, object:self)
		}
	}
	
	var showNightHours: Bool {
		get {
			return UserDefaults.standard.bool(forKey: "showNightHours")
		}
		set (newValue) {
			UserDefaults.standard.set(newValue, forKey: "showNightHours")
			NotificationCenter.default.post(name: .displaySettingsDidChange, object:self)
		}
	}
	
	var showNightWatches: Bool {
		get {
			return UserDefaults.standard.bool(forKey: "showNightWatches")
		}
		set (newValue) {
			UserDefaults.standard.set(newValue, forKey: "showNightWatches")
			NotificationCenter.default.post(name: .displaySettingsDidChange, object:self)
		}
	}
	
	func registerDefaults() {
		UserDefaults.standard.register(defaults: ["showCenterCross":true])
		UserDefaults.standard.register(defaults: ["showDayHours":true])
		UserDefaults.standard.register(defaults: ["showDayWatches":false])
		UserDefaults.standard.register(defaults: ["showNightHours":true])
		UserDefaults.standard.register(defaults: ["showNightWatches":true])
	}
}

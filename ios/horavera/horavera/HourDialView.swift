//
//  HourDial.swift
//  horavera
//
//  Created by Philip White on 11/15/18.
//  Copyright © 2018 Philip White. All rights reserved.
//

import UIKit

public class HourDialView: UIView {
	
	static let animationName = "HourDialAnimationName"
	
	var hourDialLayer: HourDialLayer {
		return layer as! HourDialLayer
	}
	
	override public class var layerClass : AnyClass {
		return HourDialLayer.self
	}
	
	public var showDialText : Bool = true {
		didSet {
			hourDialLayer.showText = showDialText
			hourDialLayer.setNeedsDisplay()
		}
	}
	
	public var showCenterCross : Bool = true {
		didSet {
			hourDialLayer.showCross = showCenterCross
			hourDialLayer.setNeedsDisplay()
		}
	}
	
	public var showDayHours : Bool = true {
		didSet {
			hourDialLayer.showDayHours = showDayHours
			hourDialLayer.setNeedsDisplay()
		}
	}
	
	public var showDayWatches : Bool = false {
		didSet {
			hourDialLayer.showDayWatches = showDayWatches
			hourDialLayer.setNeedsDisplay()
		}
	}
	
	public var showNightHours : Bool = true {
		didSet {
			hourDialLayer.showNightHours = showNightHours
			hourDialLayer.setNeedsDisplay()
		}
	}
	
	public var showNightWatches : Bool = true {
		didSet {
			hourDialLayer.showNightWatches = showNightWatches
			hourDialLayer.setNeedsDisplay()
		}
	}
		
	func toNearestEquivalentAngle(_ angle : CGFloat, reference: CGFloat) -> CGFloat {
		let diff = abs(reference - angle)
		if (diff > .pi) {
			return angle - 2.0 * .pi
		} else {
			return angle
		}
	}
	
	func toPositiveEquivalentAngle(_ angle : CGFloat) -> CGFloat {
		if (angle >= 0.0) {
			return angle
		} else {
			return 2.0 * .pi + angle
		}
	}
	
	func durationForRotation(_ startAngle : CGFloat, finishAngle : CGFloat ) -> CFTimeInterval {
		let factor : CGFloat = 0.4
		let diff = finishAngle - startAngle
		
		return CFTimeInterval(factor * diff)
	}
	
	public func setTotalDaytimeAngle(_ totalDaytimeAngle: CGFloat, currentTimeAngle: CGFloat, morningTwilightAngle: CGFloat, eveningTwilightAngle: CGFloat, animate: Bool) {
		
		hourDialLayer.totalDaytimeAngle = totalDaytimeAngle
		hourDialLayer.currentTimeAngle = currentTimeAngle
		hourDialLayer.morningTwilightAngle = morningTwilightAngle
		hourDialLayer.eveningTwilightAngle = eveningTwilightAngle
		
		if !animate {
			return
		}
		
		let presentation = hourDialLayer.presentation()!
		let fromTotalDaytimeAngle = presentation.totalDaytimeAngle
		var fromCurrentTimeAngle = presentation.currentTimeAngle
		let fromMorningTwilightAngle = presentation.morningTwilightAngle
		let fromEveningTwilightAngle = presentation.eveningTwilightAngle
		let toTotalDayTimeAngle = totalDaytimeAngle
		let toCurrentTimeAngle = currentTimeAngle
		let toMorningTwilightAngle = morningTwilightAngle
		let toEveningTwilightAngle = eveningTwilightAngle
		
		// make sure it rotates forwards if passing over sunset (2*pi or 0)
		if fromCurrentTimeAngle > toCurrentTimeAngle {
			fromCurrentTimeAngle = fromCurrentTimeAngle - 2.0 * .pi
		}
		
		let animationDuration = durationForRotation(fromCurrentTimeAngle, finishAngle: toCurrentTimeAngle)
		
		let dayTimeAngleAnimation = CABasicAnimation(keyPath: #keyPath(HourDialLayer.totalDaytimeAngle))
		dayTimeAngleAnimation.duration = animationDuration
		dayTimeAngleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
		dayTimeAngleAnimation.fromValue = fromTotalDaytimeAngle
		dayTimeAngleAnimation.toValue = toTotalDayTimeAngle
		
		let currentTimeAngleAnimation = CABasicAnimation(keyPath: #keyPath(HourDialLayer.currentTimeAngle))
		currentTimeAngleAnimation.duration = animationDuration
		currentTimeAngleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
		currentTimeAngleAnimation.fromValue = fromCurrentTimeAngle
		currentTimeAngleAnimation.toValue = toCurrentTimeAngle
		
		let morningTwilightAngleAnimation = CABasicAnimation(keyPath: #keyPath(HourDialLayer.morningTwilightAngle))
		morningTwilightAngleAnimation.duration = animationDuration
		morningTwilightAngleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
		morningTwilightAngleAnimation.fromValue = fromMorningTwilightAngle
		morningTwilightAngleAnimation.toValue = toMorningTwilightAngle
		
		let eveningTwilightAngleAnimation = CABasicAnimation(keyPath: #keyPath(HourDialLayer.eveningTwilightAngle))
		eveningTwilightAngleAnimation.duration = animationDuration
		eveningTwilightAngleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
		eveningTwilightAngleAnimation.fromValue = fromEveningTwilightAngle
		eveningTwilightAngleAnimation.toValue = toEveningTwilightAngle
		
		let animation = CAAnimationGroup()
		animation.animations = [dayTimeAngleAnimation, currentTimeAngleAnimation, morningTwilightAngleAnimation, eveningTwilightAngleAnimation]
		animation.duration = animationDuration
		animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
		hourDialLayer.add(animation, forKey: HourDialView.animationName)
	}
	
	func startWaitingRotationAnimation() {
		let animation = CABasicAnimation(keyPath: #keyPath(HourDialLayer.currentTimeAngle))
		animation.duration = 20.0
		animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
		animation.fromValue = hourDialLayer.currentTimeAngle
		
		animation.repeatCount = .greatestFiniteMagnitude
		animation.toValue = hourDialLayer.currentTimeAngle + 2.0 * .pi
		hourDialLayer.add(animation, forKey: HourDialView.animationName)
	}
	
	override public func awakeFromNib() {
		layer.contentsScale = UIScreen.main.scale
		hourDialLayer.totalDaytimeAngle = .pi
		hourDialLayer.currentTimeAngle = 0.0
		hourDialLayer.morningTwilightAngle = .pi / 8.0
		hourDialLayer.eveningTwilightAngle = .pi / 8.0
		startWaitingRotationAnimation()
	}
//
//	override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//		updateTouchLocation(touches)
//		super.touchesBegan(touches, with: event)
//	}
//
//	override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//		updateTouchLocation(touches)
//		super.touchesMoved(touches, with: event)
//	}
//
//	func updateTouchLocation(_ touches: Set<UITouch>) {
//		if (hourDialLayer.cachedNighttimeHourPaths != nil && hourDialLayer.cachedDaytimeHourPaths != nil && hourDialLayer.cachedVigilPaths != nil) {
//
//			let touch = touches.first!
//			let location = touch.location(in: self)
//
//			for (index, path) in hourDialLayer.cachedNighttimeHourPaths.enumerated() {
//				if path.contains(location) {
//					hourDialLayer.pressedPeriod = .night(index)
//					return
//				}
//			}
//
//			for (index, path) in hourDialLayer.cachedDaytimeHourPaths.enumerated() {
//				if path.contains(location) {
//					hourDialLayer.pressedPeriod = .day(index)
//					return
//				}
//			}
//
//			for (index, path) in hourDialLayer.cachedVigilPaths.enumerated() {
//				if path.contains(location) {
//					hourDialLayer.pressedPeriod = .vigil(index)
//					return
//				}
//			}
//		}
//	}
//
//	override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//		hourDialLayer.pressedPeriod = .null
//		super.touchesEnded(touches, with: event)
//	}
	
}

//
//  SRSSOrgData.swift
//  horavera
//
//  Created by Philip White on 11/27/18.
//  Copyright © 2018 Philip White. All rights reserved.
//

// SRSSOrg == sunrise-sunset.org

import Foundation
import CoreLocation

// https://api.sunrise-sunset.org/json?lat=47.605&lng=-121.9004&date=2018-11-11
// Returns times in UTC
// {"results":
// {"sunrise":"3:07:58 PM",
//  "sunset":"12:35:28 AM",
//  "solar_noon":"7:51:43 PM",
//  "day_length":"09:27:30",
//  "civil_twilight_begin":"2:34:50 PM",
//  "civil_twilight_end":"1:08:36 AM",
//  "nautical_twilight_begin":"1:57:41 PM",
//  "nautical_twilight_end":"1:45:44 AM",
//  "astronomical_twilight_begin":"1:21:31 PM",
//  "astronomical_twilight_end":"2:21:54 AM"}
//,
//"status":"OK"}

struct SRSSOrgData : Codable {
	var results : SRSSOrgResultsData
}

struct SRSSOrgResultsData : Codable {
	var sunrise : String
	var sunset : String
	var nautical_twilight_begin : String
	var nautical_twilight_end : String
}

class SRSSOrgDataHandler : DataHandler {
	func urlForRequest(for day: RelativeDay, sunData: SunData) -> URL! {
		var urlString = "https://api.sunrise-sunset.org/json?&formatted=0"
		let coordinate = sunData.location.coordinate
		urlString += "&" + latitudeQueryComponent(coordinate.latitude)
		urlString += "&" + longitudeQueryComponent(coordinate.longitude)
		urlString += "&" + dateQueryComponent(for: day, relativeTo:sunData.timeStamp)
		
		return URL(string: urlString)
	}
	
	func processResponseData(_ data: Data, day: RelativeDay )  -> ProcessedResponseData {
		do {
			let decoder = JSONDecoder()
			let srssOrgData = try decoder.decode(SRSSOrgData.self, from: data)
			
			let times = SunTimes(morningTwilight: dateFromUTCTimeString(srssOrgData.results.nautical_twilight_begin),
								 sunrise: dateFromUTCTimeString(srssOrgData.results.sunrise),
								 sunset: dateFromUTCTimeString(srssOrgData.results.sunset),
								 eveningTwilight: dateFromUTCTimeString(srssOrgData.results.nautical_twilight_end))
			
			switch day {
			case .yesterday:
				return ProcessedResponseData(yesterday: times, today: nil, tomorrow: nil)
			case .today:
				return ProcessedResponseData(yesterday: nil, today: times, tomorrow: nil)
			case .tomorrow:
				return ProcessedResponseData(yesterday: nil, today: nil, tomorrow: times)
			}
		} catch {
			print("Catch JSON Error")
			print(error)
		}
		assertionFailure()
		return ProcessedResponseData(yesterday: nil, today: nil, tomorrow: nil)

	}
	
	func dateQueryComponent(for day: RelativeDay, relativeTo baseDate: Date) -> String {
		var component = "date="
		
		let date = offsetDate(day, relativeTo:baseDate)
		
		let usLocale = Locale(identifier: "en_US")
		let template = "yyyy-MM-dd"
		let dateFormatter = DateFormatter()
		dateFormatter.locale = usLocale;
		dateFormatter.setLocalizedDateFormatFromTemplate(template);
		
		component += dateFormatter.string(from: date)
		
		return component
	}
	
	func latitudeQueryComponent(_ latitude: CLLocationDegrees) -> String {
		var latitude = latitude
		if latitude == 0 {
			latitude = 0.0000001 //Server-side bug
		}
		let component = "lat=" + String(latitude)
		return component
	}
	
	func longitudeQueryComponent(_ longitude: CLLocationDegrees) -> String {
		var longitude = longitude
		if longitude == 0 {
			longitude = 0.0000001 //Server-side bug
		}
		let component = "lng=" + String(longitude)
		return component
	}
	
	func offsetDate(_ day: RelativeDay, relativeTo baseDate: Date) -> Date {
		var date : Date?
		
		if (day == RelativeDay.yesterday) {
			date = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: baseDate)!
		} else if (day == RelativeDay.tomorrow) {
			date = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: baseDate)!
		} else {
			date = baseDate
		}
		
		return date!
	}
	
	func dateFromUTCTimeString(_ time : String) -> Date {
		let dateFormatter = ISO8601DateFormatter()
		let date = dateFormatter.date(from: time)
		return date!
	}
}

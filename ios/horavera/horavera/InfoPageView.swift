//
//  InfoPageView.swift
//  horavera
//
//  Created by Philip White on 1/8/20.
//  Copyright © 2020 Philip White. All rights reserved.
//

import SwiftUI

struct InfoPageView: View {
	var dismiss: (() -> Void)?
    var body: some View {
		VStack {
			ScrollView {
				VStack {
					Text(LocalizedStringKey("InfoPageTitle"))
						.font(.title)
						.multilineTextAlignment(.center)
					Spacer()
					Text(LocalizedStringKey("InfoPageWhatHeadline"))
						.font(.headline)
					Text(LocalizedStringKey("InfoPageWhatText"))
						.font(.body)
					Spacer()
					Text(LocalizedStringKey("InfoPageWhyHeadline"))
						.font(.headline)
					Text(LocalizedStringKey("InfoPageWhyText"))
						.font(.body)
					Spacer()
					Text(LocalizedStringKey("InfoPageHowHeadline"))
						.font(.headline)
					Text(LocalizedStringKey("InfoPageHowText"))
						.font(.body)
				}
				Spacer()
				Text(LocalizedStringKey("InfoPageSRSSLinkHeading"))
					.font(.headline)
				Button(action: {
					if let url = URL(string: "https://sunrise-sunset.org") {
						UIApplication.shared.open(url)
					}
				}) {
					Text(LocalizedStringKey("InfoPageSRSSLinkButton"))
				}
			}
			Spacer()
			Divider()
			Button(action: self.dismiss!) {
				Text(LocalizedStringKey("InfoPageBackButton"))
			}
			Spacer()
		}.padding()
    }
}

struct InfoPageView_Previews: PreviewProvider {
    static var previews: some View {
        InfoPageView()
    }
}

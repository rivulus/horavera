//
//  horaveraTests.swift
//  horaveraTests
//
//  Created by Philip White on 11/11/18.
//  Copyright © 2018 Philip White. All rights reserved.
//

import XCTest
@testable import horavera

class horaveraTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
	
	func equinoxSolarManager() -> SolarManager {
		let dateFormatter = ISO8601DateFormatter()
		let yesterdaySunset = dateFormatter.date(from: "2019-01-29T18:00:00+00:00")!
		let yesterdayEveningTwilight = yesterdaySunset.addingTimeInterval(30*60)
		let sunrise = dateFormatter.date(from: "2019-01-30T06:00:00+00:00")!
		let morningTwilight = sunrise.addingTimeInterval(-30*60)
		let sunset = dateFormatter.date(from: "2019-01-30T18:00:00+00:00")!
		let eveningTwilight = sunset.addingTimeInterval(30*60)
		let tomorrowSunrise = dateFormatter.date(from: "2019-01-31T06:00:00+00:00")!
		let tomorrowMorningTwilight = tomorrowSunrise.addingTimeInterval(-30*60)
		
		let solarManager = SolarManager(yesterdaySunset: yesterdaySunset,
										yesterdayEveningTwilight: yesterdayEveningTwilight,
										morningTwilight: morningTwilight,
										sunrise: sunrise,
										sunset: sunset,
										eveningTwilight: eveningTwilight,
										tomorrowMorningTwilight: tomorrowMorningTwilight,
										tomorrowSunrise: tomorrowMorningTwilight)
		
		return solarManager
	}
	
	func eighteenHourDaySolarManager()  -> SolarManager {
		let dateFormatter = ISO8601DateFormatter()
		let yesterdaySunset = dateFormatter.date(from: "2019-01-29T21:00:00+00:00")!
		let yesterdayEveningTwilight = yesterdaySunset.addingTimeInterval(30*60)
		let sunrise = dateFormatter.date(from: "2019-01-30T03:00:00+00:00")!
		let morningTwilight = sunrise.addingTimeInterval(-30*60)
		let sunset = dateFormatter.date(from: "2019-01-30T21:00:00+00:00")!
		let eveningTwilight = sunset.addingTimeInterval(30*60)
		let tomorrowSunrise = dateFormatter.date(from: "2019-01-31T03:00:00+00:00")!
		let tomorrowMorningTwilight = tomorrowSunrise.addingTimeInterval(-30*60)
		
		let solarManager = SolarManager(yesterdaySunset: yesterdaySunset,
										yesterdayEveningTwilight: yesterdayEveningTwilight,
										morningTwilight: morningTwilight,
										sunrise: sunrise,
										sunset: sunset,
										eveningTwilight: eveningTwilight,
										tomorrowMorningTwilight: tomorrowMorningTwilight,
										tomorrowSunrise: tomorrowMorningTwilight)
		
		return solarManager
	}
	
    func testEquinoxNoon() {
		let solarManager = equinoxSolarManager()
		let currentTime = ISO8601DateFormatter().date(from: "2019-01-30T12:00:00+00:00")
		
		let horaVera = solarManager.horaVera(currentTime!)
		
		XCTAssertEqual(horaVera!.daytimeFraction, 0.5, accuracy: 0.0001, "Daytime fraction not correct")
		XCTAssertEqual(horaVera!.stage, .dayTime, "Day stage not correct")
		XCTAssertEqual(horaVera!.hour, 6.0, accuracy: 0.0001, "Hour value not correct")
    }
	
	func testEquinoxMidnight() {
		let solarManager = equinoxSolarManager()
		let currentTime = ISO8601DateFormatter().date(from: "2019-01-30T00:00:00+00:00")
		
		let horaVera = solarManager.horaVera(currentTime!)
		
		XCTAssertEqual(horaVera!.daytimeFraction, 0.5, accuracy: 0.0001, "Daytime fraction not correct")
		XCTAssertEqual(horaVera!.stage, .nightTime, "Day stage not correct")
		XCTAssertEqual(horaVera!.hour, 6.0, accuracy: 0.0001, "Hour value not correct")
	}
	
	func testEighteenHourDayTerce() {
		// Terce = End of the third daylight-hour
		// If sunrise is at 3:00am and sunset at 9:00pm, this will be at 7:30am
		let solarManager = eighteenHourDaySolarManager()
		let currentTime = ISO8601DateFormatter().date(from: "2019-01-30T07:30:00+00:00")
		
		let horaVera = solarManager.horaVera(currentTime!)
		
		XCTAssertEqual(horaVera!.daytimeFraction, 0.75, accuracy: 0.0001, "Daytime fraction not correct")
		XCTAssertEqual(horaVera!.stage, .dayTime, "Day stage not correct")
		XCTAssertEqual(horaVera!.hour, 3.0, accuracy: 0.0001, "Hour value not correct")
	}
	
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
	
}

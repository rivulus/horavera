//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport
import DialLib

class DialViewController : UIViewController {
    override func loadView() {
		let dialView = HourDialView()
		self.view = dialView
		dialView.awakeFromNib()
	}
}
// Present the view controller in the Live View window

let viewController = DialViewController()

func renderToFileWithSize(_ size : CGFloat, multiplier : Int, layer : HourDialLayer) {
	let dimension = CGFloat(size) * CGFloat(multiplier)
	UIGraphicsBeginImageContext(CGSize(width: dimension, height: dimension))
	let context = UIGraphicsGetCurrentContext()
	layer.render(in: context!)
	let image = UIGraphicsGetImageFromCurrentImageContext()
	UIGraphicsEndImageContext()
	let imageData = image?.jpegData(compressionQuality: 1.0)
	let fileName : String = "HourDial\(size)x\(multiplier).jpeg"
	let imageURL = FileManager.default.temporaryDirectory.appendingPathComponent(fileName)
	do {
		try imageData?.write(to: imageURL)
	} catch {
		print(error)
	}
}

for (size, multiplier, showText) in [
	(60.0, 2, false), (60.0, 3, false),// iPhone App icon
	(76.0, 1, false), (76.0, 2, false), // iPad App Icon
	(83.5, 2, false),
	(1024.0, 1, false)] {
		let dimension = CGFloat(size) * CGFloat(multiplier)
		viewController.preferredContentSize = CGSize(width: dimension, height: dimension)
		
		PlaygroundPage.current.liveView = viewController
		
		//let layer : HourDialLayer = viewController.view!.layer as! HourDialLayer
		let layer = HourDialLayer()
		
		layer.frame = CGRect(x: 0, y: 0, width: dimension, height: dimension)
		layer.showText = showText
		//layer.showCross = false
		//layer.showPeriods = false
		let daytimeAngle : CGFloat = 2.0 * .pi * 5.0 / 8.0
		let nighttimeAngle = 2.0 * .pi - daytimeAngle
		layer.totalDaytimeAngle = daytimeAngle
		layer.currentTimeAngle = nighttimeAngle + .pi / 8.0
		layer.morningTwilightAngle = .pi / 8.0
		layer.eveningTwilightAngle = .pi / 8.0
		print (layer.currentTimeAngle)
		
		renderToFileWithSize(CGFloat(size), multiplier:multiplier, layer: layer)
}

print(FileManager.default.temporaryDirectory)

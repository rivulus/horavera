//
//  DialLib.h
//  DialLib
//
//  Created by Philip White on 3/5/19.
//  Copyright © 2019 Philip White. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DialLib.
FOUNDATION_EXPORT double DialLibVersionNumber;

//! Project version string for DialLib.
FOUNDATION_EXPORT const unsigned char DialLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DialLib/PublicHeader.h>


